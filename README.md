# subtitle generation from video using whisper



## Overview

Whisper is one of three components within the Graphite project:

1.Graphite-Web, a Django-based web application that renders graphs and dashboards

2.The Carbon metric processing daemons

3.The Whisper time-series database library



Whisper is a fixed-size database, similar in design and purpose to RRD (round-robin-database). It provides fast, reliable storage of numeric data over time. Whisper allows for higher resolution (seconds per point) of recent data to degrade into lower resolutions for long-term retention of historical data.

## Create a new notebook in Vertex AI

Create a new notebook in Vertex AI. Here are the steps you can follow:


1.Go to the Vertex AI Console: https://console.cloud.google.com/vertex-ai/

2.Select your project from the dropdown at the top of the page.

3.Click on "Notebooks" from the left-hand menu.

4.Click on "New Notebook" at the top of the page.

5.Select the "Custom" tab.

6.Choose your desired instance type and region.

7.Enter a name for your notebook and select a folder if needed.

8.Choose a runtime version. You can choose the latest version or a specific version depending on your requirements.

9.Choose whether you want to enable GPUs or not.

10.Click on "Create".


After creating the notebook, you can click on the "Open JupyterLab" button to open the notebook. You can also upload any data or code required for your project to the notebook.

